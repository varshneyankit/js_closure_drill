function cacheFunction(cb) {
  const cache = {};

  function callbackCaller(argument) {
    try {
      if (!cache.hasOwnProperty(argument)) {
        const result = cb(argument);
        cache[argument] = result;
        return result;
      } else {
        console.log("Argument is already present");
        return cache[argument];
      }
    } catch (error) {
      console.log("An error occured", error);
      return null;
    }
  }

  return callbackCaller;
}

module.exports = cacheFunction;
