function limitFunctionCallCount(cb, n) {
  let counter = 0;

  function callbackCaller() {
    try {
      if (counter < n) {
        counter++;
        return cb();
      } else {
        return null;
      }
    } catch (error) {
      console.log("An error occured", error);
      return null;
    }
  }

  return callbackCaller;
}

module.exports = limitFunctionCallCount;
