function counterFactory() {
  let counter = 0;
  const object = {
    increment: function () {
      try {
        counter++;
        return counter;
      } catch (error) {
        console.log("An error occured from increment method", error);
        return null;
      }
    },
    decrement: function () {
      try {
        counter--;
        return counter;
      } catch (error) {
        console.log("An error occured from decrement method", error);
        return null;
      }
    },
  };
  return object;
}

module.exports = counterFactory;
