const counterFactory = require("../counterFactory.cjs");

const object = counterFactory();

let counterValue = object.increment();
console.log(counterValue);

counterValue = object.decrement();
console.log(counterValue);
