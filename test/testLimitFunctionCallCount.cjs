const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callback() {
  return "Something!";
}

const callbackCaller = limitFunctionCallCount(callback, 4);

console.log(callbackCaller());
console.log(callbackCaller());
console.log(callbackCaller());
console.log(callbackCaller());
console.log(callbackCaller());
