const cacheFunction = require("../cacheFunction.cjs");

function callback(argument) {
  return argument * 10;
}

const callbackCaller = cacheFunction(callback);

console.log(callbackCaller(1));
console.log(callbackCaller(2));
console.log(callbackCaller(3));
console.log(callbackCaller(4));
console.log(callbackCaller(2));
console.log(callbackCaller(1));
console.log(callbackCaller(5));
